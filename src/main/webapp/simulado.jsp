<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Faça um simulado!</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/base.css">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
</head>

<body>

<!-- Navbar -->
<div class="w3-top">
    <div class="w3-bar w3-blue w3-card w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red"
           href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fas fa-bars"></i></a>

        <a href="/carregarSimulado" class="w3-bar-item w3-button w3-padding-large w3-white link-navbar">
            <i class="fas fa-pencil-alt fa-2x" aria-hidden="true"></i>
        </a>

        <a href="compras.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-shopping-cart fa-2x" aria-hidden="true"></i>
        </a>

        <a href="home.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-home fa-2x" aria-hidden="true"></i>
        </a>

        <a href="servico.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-ship fa-2x" aria-hidden="true"></i>
        </a>

        <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-phone fa-2x" aria-hidden="true"></i>
        </a>

        <a href="login.html" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white link-navbar">
            <i class="fas fa-user-secret fa-2x" aria-hidden="true"></i>
        </a>
    </div>

    <!-- Navbar on small screens -->
    <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Serviços
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Apostilas
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Simulados
        </a>

        <a href="#" class="w3-bar-item w3-button w3-padding-large">
            Fale Conosco
        </a>
    </div>
</div>

<!-- Header -->
<header class="w3-container w3-blue w3-center" style="padding:128px 16px">
    <h1 class="w3-margin w3-jumbo">Simulados do Exame de Arrais Amador</h1>
    <p class="w3-xlarge">Treine para adquirir sua habilitação!</p>
</header>

<!-- First Grid -->
<div class="w3-row-padding w3-padding-64 w3-container">
    <div class="w3-content">
        <div class="w3-twothird">
            <h1>Lista de Simulados</h1>
            <c:forEach var="simulado" items="${requestScope.listaSimulado}">
                <form action="/criarPaginaSimulado" method="post">
                    <input type="hidden" value="${simulado.id}" name="simuladoId">
                    <input type="submit" class="w3-button w3-green" value="Simulado ${simulado.id}">
                </form>
            </c:forEach>
        </div>

        <div class="w3-third w3-center" align="center">
            <figure>
                <img alt="ArraisDF" src="img/imagem_grupo.png" id="img-grupo">
            </figure>
        </div>
    </div>
</div>

<!-- Second Grid -->
<div class="w3-row-padding w3-light-grey w3-padding-64 w3-container">
    <div class="w3-content">
        <div class="w3-third w3-center">
            <figure>
                <img alt="Imagem Barco" src="img/imagem-barco.png" id="img-barco">
            </figure>
        </div>

        <div class="w3-twothird">
            <h1>Simulados</h1>
            <h5 class="w3-padding-32" align="justify">
                Treine com os simulados e se prepare logo agora para o que irá encontrar no exame de habilitação de
                Arrais Amador!
            </h5>

            <p class="w3-text-grey" align="justify">
                A prova consiste em 40 questões de múltipla escolha, devendo acertar 50% da prova para obter a sua
                habilitação.
                Não se preocupe, passar na prova é fácil e realizando os simulados, o processo é mais fácil ainda!
                Treine já!
            </p>
        </div>
    </div>
</div>

<div class="w3-container w3-black w3-center w3-opacity w3-padding-64">
    <h2 class="w3-margin w3-xlarge">jasseferreira@gmail.com - (61)3351-1971</h2>
</div>

<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">
    <div class="w3-xlarge w3-padding-32">
        <a href="https://www.facebook.com/Arraisdf/">
            <i class="fab fa-facebook w3-hover-opacity"></i>
        </a>
        <a href="https://www.instagram.com/escolanautica_arraisdf/">
            <i class="fab fa-instagram w3-hover-opacity"></i>
        </a>
    </div>

    <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>
<script type="text/javascript" src="js/libs/jquery/jquery.js"></script>
<script type="text/javascript" src="js/habilitacao.js"></script>
<script type="text/javascript" src="js/inputmask.js"></script>
<script type="text/javascript" src="js/libs/Inputmask-3.2.0/js/inputmask.extensions.js"></script>
<script type="text/javascript" src="js/libs/Inputmask-3.2.0/js/inputmask.js"></script>
<script type="text/javascript" src="js/libs/Inputmask-3.2.0/js/inputmask.numeric.extensions.js"></script>
<script type="text/javascript" src="js/libs/Inputmask-3.2.0/js/jquery.inputmask.js"></script>
</body>
</html>
