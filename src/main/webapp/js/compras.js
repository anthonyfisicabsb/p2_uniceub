"use strict";

var slideIndex = 0;
carousel();

function carousel() {
    let i = 0;
    let x = document.getElementsByClassName("imagem-apostila");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    slideIndex++;

    if (slideIndex > x.length) {
        slideIndex = 1;
    }
    x[slideIndex - 1].style.display = "block";
    setTimeout(carousel, 3500); // Change image every 2 seconds
}

function myFunction() {
    let x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") === -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}
