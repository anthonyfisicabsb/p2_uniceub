<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Escolha as datas de aula e prova!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/escolher-aula-prova.css">
</head>
<body>
<header class="w3-container w3-blue w3-center" style="padding:128px 16px">
    <h1>Datas das Aulas</h1>
    <h2 class="w3-padding-32" align="center">
        Escolha as datas em que deseja realizar as aulas teóricas e práticas!
    </h2>
</header>

<div class="w3-row-padding w3-padding-64 w3-container">
    <div class="w3-content">
        <div class="w3-third w3-center" align="center">
            <form action="/adicionarAulaProvaServlet" method="POST" enctype="multipart/form-data">
                <table id="tabela-aula-prova">
                    <tr>
                        <td><p>Data Aula Teórica</p></td>
                        <td>
                            <select name="data-aula-teorica">
                                <c:forEach var="dataAula" items="${requestScope.listaAulaTeorica}">
                                    <option value=<fmt:formatDate value="${dataAula.id}" pattern="dd/MM/yyyy"/>>
                                        <fmt:formatDate value="${dataAula.id}" pattern="dd/MM/yyyy"/>
                                            ${dataAula.local}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><p>Data Aula Prática</p></td>
                        <td>
                            <select name="data-aula-pratica">
                                <c:forEach var="data" items="${requestScope.listaAulaPratica}">
                                    <option value=<fmt:formatDate value="${data.id}" pattern="dd/MM/yyyy"/>>
                                        <fmt:formatDate value="${data.id}" pattern="dd/MM/yyyy"/>
                                            ${data.local}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><p>Data Prova</p></td>
                        <td>
                            <select name="data-prova">
                                <c:forEach var="dataProva" items="${requestScope.listaProva}">
                                    <option value=<fmt:formatDate value="${dataProva.id}" pattern="dd/MM/yyyy"/>>
                                        <fmt:formatDate value="${dataProva.id}" pattern="dd/MM/yyyy"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <input type="hidden" value="${requestScope.idCliente}" name="id-cliente">
                        <td><input type="submit" value="Pronto" class="w3-button w3-green"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

</body>
</html>
