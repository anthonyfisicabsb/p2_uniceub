package servlets;

import classes.Questao;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

@WebServlet("/carregarPagAlterarQuestao")
public class CarregarPaginaAlterarQuestao extends HttpServlet {
    public CarregarPaginaAlterarQuestao() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        QuestaoDao questaoDao = new QuestaoDao();

        Questao questao = questaoDao.findById(req.getParameter("idQuestao"), Questao.class);

        req.setAttribute("questao", questao);

        req.getRequestDispatcher("alterar-questao.jsp").forward(req, res);
    }
}
