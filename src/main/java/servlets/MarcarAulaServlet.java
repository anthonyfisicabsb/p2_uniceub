package servlets;

import classes.*;
import classes.enums.TipoAulaNome;
import classes.enums.TipoServicoNome;
import com.google.common.io.ByteStreams;
import dao.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/marcarAulaServlet")
@MultipartConfig
public class MarcarAulaServlet extends HttpServlet {

    public MarcarAulaServlet() {
        super();
    }

    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {

        Cliente cliente = new Cliente();
        cliente.setNome(request.getParameter("nomecompleto"));
        cliente.setRg(request.getParameter("rg"));
        cliente.setCpf(request.getParameter("cpf"));
        cliente.setNaturalidade(request.getParameter("naturalidade"));
        cliente.setUfNaturalidade(request.getParameter("uf-naturalidade"));
        cliente.setNomePai(request.getParameter("pai"));
        cliente.setNomeMae(request.getParameter("mae"));
        cliente.setEndereco(request.getParameter("endereco"));
        cliente.setCep(request.getParameter("cep"));
        cliente.setCidade(request.getParameter("cidade"));
        cliente.setUfEndereco(request.getParameter("uf-endereco"));
        cliente.setEmail(request.getParameter("email"));
        cliente.setTelefone(request.getParameter("telefone"));

        Part imagemComprovante = request.getPart("comprovante");

        Part imagemHabilitacao = request.getPart("habilitacao");

        cliente.setComprovanteResidencia(ByteStreams.toByteArray(imagemComprovante.getInputStream()));
        cliente.setCopiaHabilitacao(ByteStreams.toByteArray(imagemHabilitacao.getInputStream()));

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        try {
            cliente.setDataNascimento(format.parse(request.getParameter("datanascimento")));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("erro na conversão da data!", e);
        }

        ClienteDao clienteDao = new ClienteDao();
        Integer id = clienteDao.findIdByRg(cliente.getRg());

        if (id == -1) {
            clienteDao.insert(cliente);
        } else {
            cliente.setId(id);
            clienteDao.update(cliente, Cliente.class);
        }

        TipoServicoDao tipoDao = new TipoServicoDao();

        Integer idTipo = tipoDao.findIdByName("HABILITACAO");

        if (idTipo == -1) {
            TipoServico tipoNovo = new TipoServico();
            tipoNovo.setPreco(500.0);
            tipoNovo.setTipo(TipoServicoNome.HABILITACAO);
            tipoDao.insert(tipoNovo);

            idTipo = tipoDao.findIdByName("HABILITACAO");
        }

        Servico servico = new Servico();
        servico.setPago(false);
        servico.setCliente(cliente);
        servico.setTipo(tipoDao.findById(idTipo, TipoServico.class));

        ServicoDao servicoDao = new ServicoDao();

        servicoDao.insert(servico);

        AulaDao aulaDao = new AulaDao();
        ProvaDao provaDao = new ProvaDao();

        List<Aula> listaAula = aulaDao.findAll(new Aula().getTableName());
        List<Aula> listaAulaTeorica = listaAula
                .stream()
                .filter(a -> a.getTipoAula().getTipoAula() == TipoAulaNome.TEORICA)
                .collect(Collectors.toList());

        List<Aula> listaAulaPratica = listaAula
                .stream()
                .filter(a -> a.getTipoAula().getTipoAula() == TipoAulaNome.PRATICA)
                .collect(Collectors.toList());

        List<Prova> listaProva = provaDao.findAll(new Prova().getTableName());

        request.setAttribute("idCliente", cliente.getId());
        request.setAttribute("listaAulaPratica", listaAulaPratica);
        request.setAttribute("listaAulaTeorica", listaAulaTeorica);
        request.setAttribute("listaProva", listaProva);

        request.getRequestDispatcher("/escolher-aula-prova.jsp").forward(request, response);
    }
}