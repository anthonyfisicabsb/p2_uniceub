package servlets;

import classes.Questao;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

@WebServlet("/inserirQuestao")
public class InserirQuestaoServlet extends HttpServlet {
    public InserirQuestaoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        Questao questao = new Questao();
        QuestaoDao questaoDao = new QuestaoDao();

        if (questaoDao.findById(req.getParameter("enunciado"), Questao.class) == null) {

            questao.setId(req.getParameter("enunciado"));
            questao.setItemA(req.getParameter("itemA"));
            questao.setItemB(req.getParameter("itemB"));
            questao.setItemC(req.getParameter("itemC"));
            questao.setItemD(req.getParameter("itemD"));
            questao.setItemE(req.getParameter("itemE"));
            questao.setResposta(Integer.parseInt(req.getParameter("resposta")));
            questao.setTentativa(0);
            questao.setAcertos(0);

            questaoDao.insert(questao);
        }

        req.getRequestDispatcher("/carregaPaginaQuestao").forward(req, res);
    }
}
