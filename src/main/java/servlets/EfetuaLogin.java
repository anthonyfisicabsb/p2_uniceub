package servlets;

import classes.Usuario;
import dao.UsuarioDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/efetuaLogin")
public class EfetuaLogin extends HttpServlet {
    public EfetuaLogin() {
        super();
    }

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UsuarioDao usuarioDao = new UsuarioDao();

        if (usuarioDao.findById(request.getParameter("email"), Usuario.class) != null) {
            Usuario usuario = usuarioDao.findById(request.getParameter("email"), Usuario.class);

            if (usuario.getSenha().equals(request.getParameter("senha")))
                request.getRequestDispatcher("/carregaAdminPage").forward(request, response);
        } else {
            request.getRequestDispatcher("login.html").forward(request, response);
        }
    }
}
