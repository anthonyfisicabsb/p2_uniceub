package servlets;

import classes.*;
import dao.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/adicionarAulaProvaServlet")
@MultipartConfig
public class AdicionarAulaProvaServlet extends HttpServlet {

    public AdicionarAulaProvaServlet() {
        super();
    }

    @Override
    protected void service(HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date dataAulaPratica = format.parse(request.getParameter("data-aula-teorica"));
            Date dataAulaTeorica = format.parse(request.getParameter("data-aula-pratica"));
            Date dataProva = format.parse(request.getParameter("data-prova"));

            AulaDao aulaDao = new AulaDao();
            ProvaDao provaDao = new ProvaDao();
            RlClienteProvaDao rlClienteProvaDao = new RlClienteProvaDao();
            ClienteDao clienteDao = new ClienteDao();

            Aula aulaPratica = aulaDao.findById(dataAulaPratica, Aula.class);
            Aula aulaTeorica = aulaDao.findById(dataAulaTeorica, Aula.class);
            Prova prova = provaDao.findById(dataProva, Prova.class);
            Cliente cliente = clienteDao.findById(Integer.parseInt(request.getParameter("id-cliente")),
                    Cliente.class);

            RlClienteProva rlClienteProva = new RlClienteProva();

            rlClienteProva.setProva(prova);
            rlClienteProva.setCliente(cliente);
            rlClienteProva.setAprovado(false);

            rlClienteProvaDao.insert(rlClienteProva);

            RlClienteAula rlClienteAula = new RlClienteAula();
            RlClienteAula rlClienteAula2 = new RlClienteAula();

            rlClienteAula.setCliente(cliente);
            rlClienteAula2.setCliente(cliente);

            rlClienteAula.setAula(aulaPratica);
            rlClienteAula2.setAula(aulaTeorica);

            RlClienteAulaDao rlClienteAulaDao = new RlClienteAulaDao();
            rlClienteAulaDao.insert(rlClienteAula);
            rlClienteAulaDao.insert(rlClienteAula2);

        } catch (ParseException e) {
            e.printStackTrace();
            throw new ServletException("erro na conversao da data", e);
        }

        request.getRequestDispatcher("escolher-pagamento.html").forward(request, response);
    }
}
