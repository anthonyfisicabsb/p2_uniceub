package servlets;

import classes.Questao;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

@WebServlet("/alterarQuestao")
public class AlterarQuestao extends HttpServlet {
    public AlterarQuestao() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        QuestaoDao questaoDao = new QuestaoDao();
        Questao questao = new Questao();

        if (questaoDao.findById(req.getParameter("enunciado"), Questao.class) != null) {
            questao.setId(req.getParameter("enunciado"));
            questao.setItemA(req.getParameter("itemA"));
            questao.setItemB(req.getParameter("itemB"));
            questao.setItemC(req.getParameter("itemC"));
            questao.setItemD(req.getParameter("itemD"));
            questao.setItemE(req.getParameter("itemE"));
            questao.setResposta(Integer.parseInt(req.getParameter("resposta")));
            questao.setTentativa(Integer.parseInt(req.getParameter("tentativa")));
            questao.setAcertos(Integer.parseInt(req.getParameter("acertos")));

            questaoDao.update(questao, Questao.class);
        }

        req.getRequestDispatcher("/carregaPaginaQuestao").forward(req, res);
    }
}
