package servlets;

import classes.Questao;
import dao.QuestaoDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/carregaPaginaQuestao")
public class CarregarPaginaQuestao extends HttpServlet {
    public CarregarPaginaQuestao() {
        super();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        QuestaoDao questaoDao = new QuestaoDao();

        List<Questao> listaQuestao = questaoDao.findAll(new Questao().getTableName());

        req.setAttribute("questoes", listaQuestao);

        req.getRequestDispatcher("questoes.jsp").forward(req, res);
    }
}
