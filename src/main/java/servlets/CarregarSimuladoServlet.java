package servlets;

import classes.Simulado;
import dao.SimuladoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.List;

@WebServlet("/carregarSimulado")
public class CarregarSimuladoServlet extends HttpServlet {
    public CarregarSimuladoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {

        SimuladoDao simuladoDao = new SimuladoDao();

        List<Simulado> listaSimulado = simuladoDao.findAll(new Simulado().getTableName());

        req.setAttribute("listaSimulado", listaSimulado);

        req.getRequestDispatcher("simulado.jsp").forward(req, res);
    }
}
