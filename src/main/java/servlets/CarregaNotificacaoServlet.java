package servlets;

import classes.Notificacao;
import dao.NotificacaoDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

;

@WebServlet("/carregaNotificacao")
public class CarregaNotificacaoServlet extends HttpServlet {
    public CarregaNotificacaoServlet() {
        super();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        NotificacaoDao notificacaoDao = new NotificacaoDao();

        List<Notificacao> listaNotificacao = notificacaoDao.findAll(new Notificacao().getTableName());

        req.setAttribute("listaNotificacao", listaNotificacao);

        req.getRequestDispatcher("transacoes.jsp").forward(req, res);
    }
}
