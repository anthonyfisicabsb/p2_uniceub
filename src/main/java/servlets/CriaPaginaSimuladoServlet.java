package servlets;

import classes.Questao;
import dao.SimuladoDao;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.List;

@WebServlet("/criarPaginaSimulado")
public class CriaPaginaSimuladoServlet extends HttpServlet {
    public CriaPaginaSimuladoServlet() {
        super();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("simuladoId"));

        SimuladoDao simuladoDao = new SimuladoDao();

        List<Questao> listaQuestao = simuladoDao.carregarListaQuestao(id);

        req.setAttribute("questoes", listaQuestao);
        req.setAttribute("id", id);

        req.getRequestDispatcher("pagina-simulado.jsp").forward(req, res);
    }
}
