package classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "simulado")
public class Simulado extends Entitable<Simulado, Integer> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToMany
    private Set<Questao> listaQuestao = new HashSet<>();

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Questao> getListaQuestao() {
        return listaQuestao;
    }

    public void setListaQuestao(Set<Questao> listaQuestao) {
        this.listaQuestao = listaQuestao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Simulado simulado = (Simulado) o;
        return Objects.equals(id, simulado.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String getTableName() {
        return "Simulado";
    }

    @Override
    public void clonar(Simulado objeto) {

    }
}
