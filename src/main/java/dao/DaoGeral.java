package dao;

import persistence.PersistenceManager;
import javax.persistence.Query;
import javax.persistence.EntityTransaction;
import javax.persistence.EntityManager;
import java.util.List;
import classes.Entitable;

public abstract class DaoGeral<T extends Entitable, V> {
    protected PersistenceManager manager;

    public DaoGeral() {
        manager = PersistenceManager.getInstance();
    }

    public void insert(T objeto) {
        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        entityManager.persist(objeto);

        transaction.commit();

        entityManager.close();
    }

    public T findById(V id, Class<T> classe) {
        T retorno;

        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        retorno = entityManager.find(classe, id);

        transaction.commit();

        entityManager.close();

        return retorno;
    }

    public void remove(T objeto, Class<T> classe) {
        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        if (objeto != null) {
            T objetoBd = entityManager.find(classe, objeto.getId());
            entityManager.remove(objetoBd);
        }

        transaction.commit();

        entityManager.close();
    }

    public List<T> findAll(String tableName) {
        List<T> retorno;

        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();
        Query query = entityManager.createQuery("from " + tableName);
        retorno = query.getResultList();

        transaction.commit();

        entityManager.close();

        return retorno;
    }

    public void update (T objeto, Class<T> classe){
        EntityManager entityManager = manager.getEntityManager();

        final EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        if(objeto != null) {
            T objetoBd = entityManager.find(classe, objeto.getId());

            objetoBd.clonar(objeto);

        }

        transaction.commit();

        entityManager.close();
    }
}